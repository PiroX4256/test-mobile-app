const minimatch = require('minimatch');
const process = require('process');
const parse = require('lcov-parse');

function run() {
  const lcovPath = 'coverage/lcov.info';
  const minCoverage = 75;
  const excluded = '';
  const excludedFiles = excluded.split(' ');

  parse(lcovPath, (_, data) => {
    if (typeof data === 'undefined') {
      console.log('lcov path not set!');
      return;
    }

    const linesMissingCoverage = [];

    let totalFinds = 0;
    let totalHits = 0;
    data.forEach((element) => {
      if (shouldCalculateCoverageForFile(element['file'], excludedFiles)) {
        totalFinds += element['lines']['found'];
        totalHits += element['lines']['hit'];

        for (const lineDetails of element['lines']['details']) {
          const hits = lineDetails['hit'];

          if (hits === 0) {
            const fileName = element['file'];
            const lineNumber = lineDetails['line'];
            linesMissingCoverage[fileName] =
              linesMissingCoverage[fileName] || [];
            linesMissingCoverage[fileName].push(lineNumber);
          }
        }
      }
    });
    const coverage = (totalHits / totalFinds) * 100;
    const isValidBuild = coverage >= minCoverage;
    if (!isValidBuild) {
      const linesMissingCoverageByFile = Object.entries(
        linesMissingCoverage
      ).map(([file, lines]) => {
        return `${file}: ${lines.join(', ')}`;
      });

      console.log(
        `${coverage} is less than min_coverage ${minCoverage}\n\n` +
          'Lines not covered:\n' +
          linesMissingCoverageByFile.map((line) => `  ${line}`).join('\n')
      );
    }
  });
}

function shouldCalculateCoverageForFile(fileName, excludedFiles) {
  for (let i = 0; i < excludedFiles.length; i++) {
    const isExcluded = minimatch(fileName, excludedFiles[i]);
    if (isExcluded) {
      console.log(`Excluding ${fileName} from coverage`);
      return false;
    }
  }
  return true;
}

run();
